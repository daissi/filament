if(NOT TARGET jsm)
    find_path(jsmn_INCLUDE_DIR NAMES jsmn.h)
    add_library(jsmn INTERFACE)
    target_include_directories(jsmn INTERFACE ${jsmn_INCLUDE_DIR})
endif()

