# Empty
find_package(PkgConfig REQUIRED)
if(NOT TARGET glslang)
    pkg_check_modules(glslang REQUIRED IMPORTED_TARGET spirv glslang)
    add_library(glslang INTERFACE)
    target_include_directories(glslang INTERFACE
            /usr/include/glslang/Public
            /usr/include/glslang/Include
            /usr/include/glslang/MachineIndependent
            /usr/include/glslang/SPIRV
            )
    set_property(TARGET PkgConfig::glslang APPEND PROPERTY INTERFACE_LINK_LIBRARIES ${STDCXX_LIBRARY})
    target_link_libraries(glslang INTERFACE PkgConfig::glslang)
endif()
