if(NOT TARGET filament::imgui)
    find_path(imgui_INCLUDE_DIR NAMES imgui.h PATH_SUFFIXES imgui)
    find_library(imgui_LIBRARY NAMES imgui)
    if(imgui_INCLUDE_DIR AND imgui_LIBRARY)
        add_library(filament::imgui INTERFACE IMPORTED)
        set_target_properties(filament::imgui PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${imgui_INCLUDE_DIR}"
            INTERFACE_LINK_LIBRARIES "${imgui_LIBRARY}"
        )
    endif()
endif()
