filament (1.9.25+dfsg2-9) unstable; urgency=medium

  * Disable debug code

 -- Timo Röhling <roehling@debian.org>  Wed, 23 Nov 2022 23:32:15 +0100

filament (1.9.25+dfsg2-8) unstable; urgency=medium

  * Fix compatibility with glslang-dev >= 11.12.0
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Sat, 12 Nov 2022 13:20:14 +0100

filament (1.9.25+dfsg2-7) unstable; urgency=medium

  * Stop using the deprecated DRACO_LIBRARIES variable
  * Switch from GLEW to libepoxy

 -- Timo Röhling <roehling@debian.org>  Thu, 03 Nov 2022 00:09:28 +0100

filament (1.9.25+dfsg2-6) unstable; urgency=medium

  * Workaround for armel clang compiler bug.
    Thanks to Adrian Bunk
  * Ignore dh_dwz failure with clang-14

 -- Timo Röhling <roehling@debian.org>  Wed, 27 Jul 2022 22:13:33 +0200

filament (1.9.25+dfsg2-5) unstable; urgency=medium

  * Fix FTBFS with GCC-12 (Closes: #1012926)

 -- Timo Röhling <roehling@debian.org>  Thu, 16 Jun 2022 18:58:59 +0200

filament (1.9.25+dfsg2-4) unstable; urgency=medium

  * Ensure proper alignment of resgen _OFFSET and _SIZE variables

 -- Timo Röhling <roehling@debian.org>  Sun, 13 Feb 2022 01:07:53 +0100

filament (1.9.25+dfsg2-3) unstable; urgency=medium

  * Disable LTO.
    There seems to be an issue with link time optimization on Ubuntu

 -- Timo Röhling <roehling@debian.org>  Fri, 11 Feb 2022 00:48:35 +0100

filament (1.9.25+dfsg2-2) unstable; urgency=medium

  * Fix GLEW initialization

 -- Timo Röhling <roehling@debian.org>  Thu, 10 Feb 2022 12:48:59 +0100

filament (1.9.25+dfsg2-1) unstable; urgency=medium

  [ Adrian Bunk ]
  * Link with libatomic on architectures where it is needed

  [ Timo Röhling ]
  * New upstream version 1.9.25+dfsg2
    - Remove spirv-cross from source tarball, it is no longer needed

 -- Timo Röhling <roehling@debian.org>  Wed, 09 Feb 2022 10:57:06 +0100

filament (1.9.25+dfsg-8) unstable; urgency=medium

  * Avoid mips as identifier because it fails on mips* arch

 -- Timo Röhling <roehling@debian.org>  Mon, 07 Feb 2022 10:31:42 +0100

filament (1.9.25+dfsg-7) unstable; urgency=medium

  * Work around M_PIf name collision with glibc 2.34

 -- Timo Röhling <roehling@debian.org>  Mon, 07 Feb 2022 10:12:42 +0100

filament (1.9.25+dfsg-6) unstable; urgency=medium

  * Use system spirv-cross

 -- Timo Röhling <roehling@debian.org>  Sun, 06 Feb 2022 22:59:40 +0100

filament (1.9.25+dfsg-5) unstable; urgency=medium

  * Enable Vulkan on 64 bit architectures only.
    Filament assumes that VkSurfaceKHR is a pointer type, which is
    only true on 64 bit architectures.

 -- Timo Röhling <roehling@debian.org>  Thu, 27 Jan 2022 12:39:04 +0100

filament (1.9.25+dfsg-4) unstable; urgency=medium

  * Switch to clang default version

 -- Timo Röhling <roehling@debian.org>  Wed, 26 Jan 2022 23:15:57 +0100

filament (1.9.25+dfsg-3) unstable; urgency=medium

  * Rename resgen executable to avoid conflict with mono-devel
    (Closes: #1004380)
  * Replace BlueGL with GLEW

 -- Timo Röhling <roehling@debian.org>  Wed, 26 Jan 2022 18:23:13 +0100

filament (1.9.25+dfsg-2) unstable; urgency=medium

  * Fix draco linkage
  * Make ibl_lite a shared library
  * Fix libstdc++ compat

 -- Timo Röhling <roehling@debian.org>  Tue, 25 Jan 2022 22:12:20 +0100

filament (1.9.25+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #974734)

 -- Timo Röhling <roehling@debian.org>  Sun, 24 Oct 2021 10:33:22 +0200
